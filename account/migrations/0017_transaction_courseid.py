# Generated by Django 2.0.5 on 2018-06-03 21:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0016_transaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='courseID',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='account.Course'),
            preserve_default=False,
        ),
    ]
