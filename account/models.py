from django.db import models
from django.conf import settings


class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class Profile(models.Model):
    Role = (
        ('D', 'Driver'),
        ('C', 'Client')
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d',
                              blank=True)
    role = models.CharField(max_length=1, choices=Role)
    isDriver = models.BooleanField(default=False)
    isClient = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.user)


class Car(models.Model):
    brand = models.CharField(max_length=32)  # VARCHAR(32)
    car_model = models.CharField(max_length=16)  # VARCHAR(16)
    register_number = models.CharField(max_length=16, unique=True)  # VARCHAR(16)
    number_of_seats = IntegerRangeField(min_value=1, max_value=7)  # SMALLINT
    isTaken = models.BooleanField(default=False)

    def __str__(self):
        return "Brand: %s Model: %s Register Number: %s" % (
            self.brand, self.car_model, self.register_number)


class DriverDetail(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    GENDER = (
        ('M', 'Man'),
        ('W', 'Woman'),
    )
    gender = models.CharField(max_length=1, choices=GENDER, null=True, blank=True)
    isBusy = models.BooleanField(default=False)
    CarID = models.ForeignKey('Car', on_delete=models.CASCADE, null=True, blank=True)  # BIGINT


class Course(models.Model):
    Availability = (
        ('A', 'Available'),
        ('T', 'Taken'),
        ('D', 'Done'),
        ('C', 'Cancelled')
    )
    carID = models.ForeignKey('Car', on_delete=models.CASCADE, null=True, blank=True)  # BIGINT
    driverID = models.ForeignKey('Profile', related_name='Driver', on_delete=models.CASCADE, null=True, blank=True)  # BIGINT
    clientID = models.ForeignKey('Profile', related_name='Client', on_delete=models.CASCADE, null=True)  # BIGINT
    startTime = models.DateTimeField()
    endTime = models.DateTimeField(null=True, blank=True)
    fromPos = models.TextField(max_length=20)
    toPos = models.TextField(max_length=20, null=True)
    cost = models.FloatField(default=0, null=True)
    available = models.CharField(max_length=1, choices=Availability, default='A')

    def __str__(self):
        return 'Z: ' + self.fromPos + ' Do: ' + self.toPos + ' O: ' + \
               self.startTime.strftime("%Y-%m-%d %H:%M:%S")


class CourseContainer(models.Model):
    course = models.OneToOneField(Course, on_delete=models.CASCADE,)


class Transaction(models.Model):
    driverID = models.ForeignKey('Profile', on_delete=models.CASCADE, )  # BIGINT
    courseID = models.ForeignKey('Course', on_delete=models.CASCADE)
    change = models.TextField(max_length=120)  # VARCHAR
    reason = models.TextField(max_length=120)  # VARCHAR
    date = models.DateField()  # DATE
