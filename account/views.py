from django.shortcuts import render
import datetime
from django.contrib.auth.decorators import login_required
from .forms import *
from .models import Profile, DriverDetail
from django.contrib.auth.models import Group
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
import random


@login_required
def dashboard(request):
    profile = request.user.profile
    if profile.isDriver:
        driver_details = DriverDetail.objects.get(user=request.user)
        try:
            actuall_course = Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T')[0]
        except IndexError as e:
            actuall_course = None
            driver_details.isBusy = False
            driver_details.save()

        return render(request,
                      'account/dashboard.html',
                      {'section': 'dashboard',
                       'profile': profile,
                       'driver_detail': driver_details,
                       'actuall_course': actuall_course})
    else:
        if Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='T') | Q(available='A')).count()>0:
            actuall_course = Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='T') | Q(available='A'))[0]
            course_starus = actuall_course.available
            course_driver = actuall_course.driverID
        else:
            actuall_course = None
            course_starus = None
            course_driver = None
        return render(request,
                      'account/dashboard.html',
                      {'section': 'dashboard',
                       'profile': profile,
                       'actuall_course': actuall_course,
                       'course_status': course_starus,
                       'course_driver': course_driver,
                       })


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'account/register.html',
                  {'user_form': user_form})


def index(request):
    return render(
        request,
        'account/index.html'
    )


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        profile_form = ProfileEditForm(
            instance=request.user.profile,
            data=request.POST,
            files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Uaktualnienie profilu' \
                                      'zakończyło się sukcesem.')
        else:
            messages.error(request, 'Wystąpił błąd poczas uaktualniania profilu.')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,
                  'account/edit.html',
                  {'user_form': user_form,
                   'profile_form': profile_form})


def register_driver(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            profile.isDriver = True
            profile.save()
            driver_detail = DriverDetail.objects.create(user=new_user)
            my_group = Group.objects.get(name='Drivers')
            my_group.user_set.add(new_user)
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'account/register.html',
                  {'user_form': user_form})


def register_client(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            profile.isClient = True
            profile.role = 'C'
            profile.save()
            my_group = Group.objects.get(name='Clients')
            my_group.user_set.add(new_user)
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'account/register.html',
                  {'user_form': user_form})


@login_required
def change_car(request):
    if request.method == 'POST':
        car_form = CarEditForm(instance=DriverDetail.objects.get(user=request.user),
                               data=request.POST,
                               files=request.FILES)

        if car_form.is_valid():
            try:
                previous_car = Car.objects.get(id=DriverDetail.objects.get(user=request.user).CarID.id)
                previous_car.isTaken = False
                previous_car.save()
            except ObjectDoesNotExist as e:
                pass
            except AttributeError as e:
                pass

            try:
                actual_car = Car.objects.get(id=car_form.cleaned_data['CarID'].id)
                actual_car.isTaken = True
                actual_car.save()
            except AttributeError as e:
                DriverDetail.objects.get(user=request.user).CarID = None

            car_form.save()
            messages.success(request, 'Pomyślnie zmieniono samochód.')
        else:
            messages.error(request, 'Wystąpił błąd poczas zmiany samochodu.')
    else:
        car_form = CarEditForm(instance=DriverDetail.objects.get(user=request.user))

    return render(request,
                  'account/change-car.html',
                  {'car_form': car_form})


@login_required
def add_car(request):
    if request.method == 'POST':
        car_form = CarAddForm(request.POST)
        if car_form.is_valid():
            new_car = car_form.save(commit=False)
            new_car.save()
            messages.success(request, 'Pomyślnie dodano samochód')
    else:
        car_form = CarAddForm()
    return render(request,
                  'account/add_car.html',
                  {'car_form': car_form})


@login_required
def choose_course(request):
    if request.method == 'POST':
        choose_form = ChooseCourse(request.user, request.POST)
        if choose_form.is_valid():
            if 'details' in request.POST:
                return_msg=""
                if choose_form.cleaned_data['course'].driverID:
                    driver = User.objects.get(id=choose_form.cleaned_data['course'].driverID.user.id)
                    return_msg += 'Kierowca: ' + driver.first_name + ' ' + driver.last_name + '<br>'

                client = User.objects.get(id=choose_form.cleaned_data['course'].clientID.user.id)
                if choose_form.cleaned_data['course'].carID:
                    car = Car.objects.get(id=choose_form.cleaned_data['course'].carID.id)
                    return_msg += 'Samochód ' + car.brand + ' ' + car.car_model + '<br>'
                startTime = choose_form.cleaned_data['course'].startTime
                avail = choose_form.cleaned_data['course'].available
                messages.info(request, return_msg
                              + 'Klient: ' + client.first_name + ' ' + client.last_name + '<br>'
                              + 'Czas rozpoczęcia ' + startTime.strftime("%Y-%m-%d %H:%M:%S") + '<br>'
                              + 'Kurs ma status: ' + avail)
            else:
                if Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T').count() < 1:
                    choosed_course = Course.objects.get(id=choose_form.cleaned_data['course'].id)
                    # ToDo: dodac tutaj branie kursu
                    choosed_course.available = 'T'
                    choosed_course.save()
                    driver = DriverDetail.objects.get(user=request.user)
                    choosed_course.driverID = Profile.objects.get(user=driver.user)
                    choosed_course.carID = driver.CarID
                    choosed_course.save()
                    driver.isBusy = True
                    driver.save()
                    messages.success(request, 'Kurs został przypisany.')
                else:
                    messages.error(request, 'Możesz mieć tylko jeden zabrany kurs.')
    else:
        choose_form = ChooseCourse(request.user)
    return render(request,
                  'account/choose_form.html',
                  {'choose_form': choose_form})


@login_required
def add_course(request):
    if request.method == 'POST':
        course_form = CourseAddForm(request.POST)
        if course_form.is_valid():
            print(Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='A') | Q(available='T')).count())
            if Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='A') | Q(available='T')).count() > 0:
                messages.error(request, 'Masz już jeden zamówiony kurs.')
            else:
                new_course = course_form.save(commit=False)
                new_course.clientID = Profile.objects.get(user=request.user)
                new_course.endTime = None
                new_course.save()
                added_course = Course.objects.get(id=new_course.id)
                added_course.clientID = Profile.objects.get(user=request.user)
                added_course.save()
                messages.success(request, 'Pomyślnie dodano kurs')
    else:
        course_form = CourseAddForm()
    return render(request,
                  'account/add_course.html',
                  {'course_form': course_form,
                   })


@login_required
def cancel_course(request):
    if request.method == 'POST':
        transaction_form = TransactionForm(request.POST)
        if transaction_form.is_valid():
            if Profile.objects.get(user=request.user).isDriver:
                if Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T').count()>0:
                    actuall_course = Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T')[0]
                    actuall_course.endTime = datetime.datetime.now()
                    actuall_course.available = 'C'
                    actuall_course.save()
                    driver = DriverDetail.objects.get(user=request.user)
                    driver.isBusy = False
                    driver.save()
                    new_transaction = transaction_form.save(commit=False)
                    new_transaction.date = actuall_course.endTime
                    new_transaction.driverID = Profile.objects.get(user=driver.user)
                    new_transaction.courseID = actuall_course
                    new_transaction.save()
                    messages.success(request, 'Kurs został anulowany.')
    else:
        transaction_form = TransactionForm()
    return render(request,
                  'account/cancel_course.html',
                  {'transaction_form': transaction_form})


@login_required
def client_cancel_course(request):
    if request.method == 'POST':
        if Profile.objects.get(user=request.user).isClient:
            if Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='T')|Q(available='A')).count()>0:
                actuall_course = Course.objects.filter(clientID=Profile.objects.get(user=request.user)).filter(Q(available='T')|Q(available='A'))[0]
                actuall_course.endTime = datetime.datetime.now()
                actuall_course.available = 'C'
                actuall_course.save()
                if actuall_course.driverID:
                    driver = actuall_course.driverID
                    driver.isBusy = False
                    driver.save()

                messages.success(request, 'Kurs został anulowany.')

    transaction_form = None
    return render(request,
                  'account/cancel_course.html',
                  {'transaction_form': transaction_form})


@login_required
def show_driver_ended_cousres(request):
    if request.method == 'POST':
        show_form = ShowDriverEndedCourses(request.user, request.POST)
        if show_form.is_valid():
            if 'details' in request.POST:
                showed_form = show_form.save(commit=False)
                driver = User.objects.get(id=show_form.cleaned_data['course'].driverID.user.id)
                client = User.objects.get(id=show_form.cleaned_data['course'].clientID.user.id)
                car = Car.objects.get(id=show_form.cleaned_data['course'].carID.id)
                startTime = show_form.cleaned_data['course'].startTime
                endTime = show_form.cleaned_data['course'].endTime
                avail = show_form.cleaned_data['course'].available
                trans_msg = ""
                try:
                    transaction = Transaction.objects.get(courseID=showed_form.course)
                    trans_msg += transaction.reason
                except ObjectDoesNotExist as e:
                    pass

                messages.info(request, 'Kierowca: ' + driver.first_name + ' ' + driver.last_name + '<br>'
                              + 'Klient: ' + client.first_name + ' ' + client.last_name + '<br>'
                              + 'Samochód: ' + car.brand + ' ' + car.car_model + ' ' + car.register_number + '<br>'
                              + 'Czas rozpoczęcia: ' + startTime.strftime("%Y-%m-%d %H:%M:%S") + '<br>'
                              + 'Czas zakończenia: ' + endTime.strftime("%Y-%m-%d %H:%M:%S") + '<br>'
                              + 'Kurs ma status: ' + avail + '<br>'
                              + trans_msg )
    else:
        show_form = ShowDriverEndedCourses(request.user)
    return render(request,
                  'account/show_ended_courses.html',
                  {'show_form': show_form,
                   })


@login_required
def show_client_ended_cousres(request):
    if request.method == 'POST':
        show_form = ShowClientEndedCourses(request.user, request.POST)
        if show_form.is_valid():
            if 'details' in request.POST:
                return_msg = ""
                showed_form = show_form.save(commit=False)
                if show_form.cleaned_data['course'].driverID:
                    driver = User.objects.get(id=show_form.cleaned_data['course'].driverID.user.id)
                    return_msg += 'Kierowca: ' + driver.first_name + ' ' + driver.last_name + '<br>'
                else:
                    driver = None
                client = User.objects.get(id=show_form.cleaned_data['course'].clientID.user.id)
                if show_form.cleaned_data['course'].carID:
                    car = Car.objects.get(id=show_form.cleaned_data['course'].carID.id)
                    return_msg += 'Samochód: ' + car.brand + ' ' + car.car_model + ' ' + car.register_number + '<br>'
                else:
                    car = None
                startTime = show_form.cleaned_data['course'].startTime
                endTime = show_form.cleaned_data['course'].endTime
                avail = show_form.cleaned_data['course'].available

                messages.info(request, return_msg
                              + 'Klient: ' + client.first_name + ' ' + client.last_name + '<br>'
                              + 'Czas rozpoczęcia: ' + startTime.strftime("%Y-%m-%d %H:%M:%S") + '<br>'
                              + 'Czas zakończenia: ' + endTime.strftime("%Y-%m-%d %H:%M:%S") + '<br>'
                              + 'Kurs ma status: ' + avail + '<br>')
    else:
        show_form = ShowClientEndedCourses(request.user)
    return render(request,
                  'account/show_ended_courses.html',
                  {'show_form': show_form,
                   })


@login_required
def end_course(request):
    if request.method == 'POST':
        if Profile.objects.get(user=request.user).isDriver:
            if Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T').count()>0:
                actuall_course = Course.objects.filter(driverID=Profile.objects.get(user=request.user)).filter(available='T')[0]
                actuall_course.endTime = datetime.datetime.now()
                actuall_course.available = 'D'
                start_time = datetime.datetime(year=actuall_course.startTime.year, month=actuall_course.startTime.month,
                                               day=actuall_course.startTime.day, hour=actuall_course.startTime.hour,
                                               minute=actuall_course.startTime.minute, second=actuall_course.startTime.second)
                end_time = datetime.datetime(year=actuall_course.endTime.year, month=actuall_course.endTime.month,
                                               day=actuall_course.endTime.day, hour=actuall_course.endTime.hour,
                                               minute=actuall_course.endTime.minute, second=actuall_course.endTime.second)

                actuall_course.cost = round(float(2.0 + (end_time - start_time).seconds/60 * 1.35), 2)
                actuall_course.save()
                print((end_time - start_time).seconds)
                driver = DriverDetail.objects.get(user=request.user)
                driver.isBusy = False
                driver.save()
                messages.success(request, 'Kurs został zakończony. <br>' +
                                          'Koszt kursu: ' + str(actuall_course.cost) + '$')

    return render(request,
                  'account/end_course.html')



import threading
def checkAvailableCoursesWithoutDriver():
    drivers = DriverDetail.objects.filter(isBusy=False)
    available_courses = Course.objects.filter(available='A')
    print('Drivers: ' + str(drivers.count()))
    print('Courses: ' + str(available_courses.count()))
    for course in available_courses:
        start_time = datetime.datetime(year=course.startTime.year, month=course.startTime.month, day=course.startTime.day, hour=course.startTime.hour, minute=course.startTime.minute, second=course.startTime.second)
        present = datetime.datetime.now()
        diff = present - start_time
        print(start_time)
        print(present)
        print(diff.seconds/60)
        diff_in_mins = diff.seconds/60
        if diff_in_mins > 25 and drivers.count() > 0:
            choosed_driver = DriverDetail.objects.filter(isBusy=False).order_by('?').first()
            selected_driver = Profile.objects.get(user = choosed_driver.user)
            course.driverID = selected_driver
            course.carID = choosed_driver.CarID
            course.available = 'T'
            choosed_driver.isBusy = True
            choosed_driver.save()
            course.save()
            print(selected_driver.user.first_name)

    threading.Timer(5.0, checkAvailableCoursesWithoutDriver).start()