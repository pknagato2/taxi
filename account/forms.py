from django.contrib.auth.models import User
from django import forms
from bootstrap_datepicker_plus import DatePickerInput
from .models import *


class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ('change', 'reason')


class ShowDriverEndedCourses(forms.ModelForm):
    class Meta:
        model = CourseContainer
        fields = ('course',)

    def __init__(self, userr, *args, **kwargs):
        super(ShowDriverEndedCourses, self).__init__(*args, **kwargs)
        self.fields['course'].queryset = Course.objects.filter(
            driverID=Profile.objects.get(user=userr).id).exclude(available='A')


class ShowClientEndedCourses(forms.ModelForm):
    class Meta:
        model = CourseContainer
        fields = ('course',)

    def __init__(self, userr, *args, **kwargs):
        super(ShowClientEndedCourses, self).__init__(*args, **kwargs)
        self.fields['course'].queryset = Course.objects.filter(
            clientID=Profile.objects.get(user=userr).id).exclude(available='A')


class ChooseCourse(forms.ModelForm):
    class Meta:
        model = CourseContainer
        fields = ('course',)

    def __init__(self, userr, *args, **kwargs):
        super(ChooseCourse, self).__init__(*args, **kwargs)
        self.fields['course'].queryset = Course.objects.filter(
            driverID=Profile.objects.get(user=userr).id).filter(available='A') | Course.objects.filter(driverID=None).filter(available='A')


class CourseAddForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['id', 'driverID', 'carID', 'fromPos', 'toPos', 'startTime',]
        widgets = {
            'startTime': DatePickerInput(
                options={
                    "format": "MM/DD/YYYY HH:mm", # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super(CourseAddForm, self).__init__(*args, **kwargs)
        self.fields['driverID'].queryset = Profile.objects.filter(isDriver=True)
        self.fields['carID'].queryset = Car.objects.filter(isTaken=False)


class CarAddForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = ('brand', 'car_model', 'register_number', 'number_of_seats')


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Hasło',
                               widget=forms.PasswordInput)
    password2 = forms.CharField(label='Hasło',
                                widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError("Hasła nie są identyczne.")
        return cd['password2']


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('date_of_birth', 'photo')


class CarEditForm(forms.ModelForm):
    class Meta:
        model = DriverDetail
        fields = ('CarID',)

    def __init__(self, *args, **kwargs):
        super(CarEditForm, self).__init__(*args, **kwargs)
        self.fields['CarID'].queryset = Car.objects.filter(isTaken=False)
