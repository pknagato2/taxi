from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views
from django.conf import settings
from django.conf.urls.static import static
import schedule
from .views import checkAvailableCoursesWithoutDriver


urlpatterns = [
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^password-change/$', auth_views.password_change, name='password_change'),
    url(r'^password-change/done/$', auth_views.password_change_done, name='password_change_done'),
    url(r'^password-reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password-reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$',
        auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^password-reset/complete/$',
        auth_views.password_reset_complete,
        name='password_reset_complete'),
    url(r'^register-driver/$', views.register_driver, name='register_driver'),
    url(r'^register-client/$', views.register_client, name='register_client'),
    url(r'^register/$', views.register, name='register'),
    url(r'^home/$', views.index, name='home'),
    url(r'^change-car/$', views.change_car, name='change_car'),
    url(r'^edit/$', views.edit, name='edit'),
    url(r'^add-car/$', views.add_car, name='add_car'),
    url(r'^choose-course/$', views.choose_course, name='choose_course'),
    url(r'^add-course/$', views.add_course, name='add_course'),
    url(r'^client-cancel-course/$', views.client_cancel_course, name='client_cancel_course'),
    url(r'^cancel-course/$', views.cancel_course, name='cancel_course'),
    url(r'^end-course/$', views.end_course, name='end_course'),
    url(r'^show-driver-ended-course/$', views.show_driver_ended_cousres, name='show_ended_driver_course'),
    url(r'^show-client-ended-course/$', views.show_client_ended_cousres, name='show_ended_client_course'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)



checkAvailableCoursesWithoutDriver()