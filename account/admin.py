from django.contrib import admin
from .models import *
# Register your models here.


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'date_of_birth', 'photo']


class DriverDetailAdmin(admin.ModelAdmin):
    list_display = ['user', 'gender', 'isBusy', 'CarID']


class CarDetailAdmin(admin.ModelAdmin):
    list_display = ['id', 'brand', 'car_model', 'register_number']


class CourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'driverID', 'clientID']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(DriverDetail, DriverDetailAdmin)
admin.site.register(Car, CarDetailAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(CourseContainer)
admin.site.register(Transaction)